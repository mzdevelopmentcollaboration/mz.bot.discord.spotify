#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/runtime:5.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["Mz.Bot.Discord.Spotify/Mz.Bot.Discord.Spotify.csproj", "Mz.Bot.Discord.Spotify/"]
RUN dotnet restore "Mz.Bot.Discord.Spotify/Mz.Bot.Discord.Spotify.csproj"
COPY . .
WORKDIR "/src/Mz.Bot.Discord.Spotify"
RUN dotnet build "Mz.Bot.Discord.Spotify.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Mz.Bot.Discord.Spotify.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Mz.Bot.Discord.Spotify.dll"]
using System.IO;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

using Discord.WebSocket;

// ReSharper disable InconsistentNaming
// ReSharper disable AssignNullToNotNullAttribute

namespace Mz.Bot.Discord.Spotify
{
    public class Program
    {
        public static DiscordSocketClient _discordSocketClient = new();

        public static void Main(string[] args)
        {
            var databaseDirectory = Path.Combine(
                Directory.GetCurrentDirectory(),
                "database");
            if (!Directory.Exists(databaseDirectory))
                Directory.CreateDirectory(databaseDirectory);
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices(services =>
                {
                    services.AddHostedService<BotWorker>();
                    services.AddHostedService<MonitoringWorker>();
                });
    }
}

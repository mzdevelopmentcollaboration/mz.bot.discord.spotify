using System;
using System.IO;
using System.Linq;
using System.Timers;
using System.Threading;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using LiteDB;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

using Mz.Bot.Discord.Spotify.Models.Spotify;
using Mz.Bot.Discord.Spotify.Methods.Spotify;
using Mz.Bot.Discord.Spotify.Models.Settings;

using Timer = System.Timers.Timer;

// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable MemberCanBeMadeStatic.Local

namespace Mz.Bot.Discord.Spotify
{
    public class BotWorker : BackgroundService
    {
        private Timer _spotifyApiTokenRefreshTimer;
        private readonly ILogger<BotWorker> _logger;
        private readonly Configuration _config = new();
        private readonly ServiceProvider _discordServiceProvider;
        private readonly CommandService _discordCommandService = new();

        public BotWorker(ILogger<BotWorker> logger, IConfiguration config)
        {
            _logger = logger;
            config.Bind(_config);
            _discordServiceProvider = new ServiceCollection().Configure<Configuration>(config)
                .AddTransient(x => x.GetRequiredService<IOptions<Configuration>>().Value)
                .BuildServiceProvider();
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation(_config.Locale.LogMessages.BotStartup);
            SetupSpotifyApiTokenRefresh();
            _spotifyApiTokenRefreshTimer = new Timer(3500000);
            _spotifyApiTokenRefreshTimer.Elapsed += SpotifyApiTokenRefreshTimerOnElapsed;
            _spotifyApiTokenRefreshTimer.Enabled = true;
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                Program._discordSocketClient.MessageReceived += DiscordSocketClientOnMessageReceived;
                Program._discordSocketClient.Ready += SetupMetaData;
                await _discordCommandService.AddModulesAsync(Assembly.GetEntryAssembly(), _discordServiceProvider);
                await Program._discordSocketClient.LoginAsync(TokenType.Bot,
                    Environment.GetEnvironmentVariable("discordApplicationSecret"));
                await Program._discordSocketClient.StartAsync();
                await Task.Delay(-1, stoppingToken);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation(_config.Locale.LogMessages.BotShutdown);
            return base.StopAsync(cancellationToken);
        }
        
        private async Task DiscordSocketClientOnMessageReceived(SocketMessage messageParam)
        {
            if (messageParam is not SocketUserMessage message)
            {
                return;
            }
            var commandIndicatorPosition = 0;
            if (!(message.HasCharPrefix('!', ref commandIndicatorPosition) ||
                  message.HasMentionPrefix(Program._discordSocketClient.CurrentUser, ref commandIndicatorPosition)) ||
                message.Author.IsBot)
            {
                return;
            }
            var context = new SocketCommandContext(Program._discordSocketClient, message);
            await _discordCommandService.ExecuteAsync(context, commandIndicatorPosition, _discordServiceProvider);
        }

        private async Task SetupMetaData()
        {
            await Program._discordSocketClient.SetGameAsync(_config.Locale.InfoMessages.BotStatus);
        }

        private void SpotifyApiTokenRefreshTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            SetupSpotifyApiTokenRefresh();
            _logger.LogInformation(_config.Locale.LogMessages.SpotifyApiTokenRefresh);
        }

        private void SetupSpotifyApiTokenRefresh()
        {
            var spotifyApiManager = new SpotifyApiManager();
            var spotifyAuthenticationInformation = spotifyApiManager
                .FetchSpotifyApiAuthenticationToken(Environment.GetEnvironmentVariable("spotifyApiClientId"),
                    Environment.GetEnvironmentVariable("spotifyApiClientSecret"));
            using var spotifyPlaylistDatabase = new LiteDatabase(Path.Combine(Directory.GetCurrentDirectory(),
                "database", _config.Database.DatabaseName));
            var spotifyAuthenticationInformationCollection =
                spotifyPlaylistDatabase.GetCollection<SpotifyAuthenticationInformation>(
                    _config.Database.AuthenticationInformationCollectionName);
            var spotifyAuthenticationInformationCollectionItems =
                spotifyAuthenticationInformationCollection.FindAll();
            if (spotifyAuthenticationInformationCollectionItems.Any())
            {
                spotifyAuthenticationInformationCollection.DeleteAll();
            }
            spotifyAuthenticationInformationCollection.Insert(spotifyAuthenticationInformation);
        }
    }
}

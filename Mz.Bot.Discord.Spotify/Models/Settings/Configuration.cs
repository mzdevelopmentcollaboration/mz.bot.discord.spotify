﻿using Newtonsoft.Json;

namespace Mz.Bot.Discord.Spotify.Models.Settings
{
    public class LogLevel
    {
        public string Default { get; set; }
        public string Microsoft { get; set; }

        [JsonProperty("Microsoft.Hosting.Lifetime")]
        public string MicrosoftHostingLifetime { get; set; }
    }

    public class Logging
    {
        public LogLevel LogLevel { get; set; }
    }

    public class ErrorMessages
    {
        public string PlaylistItemAdd { get; set; }
        public string DeletedChannelTarget { get; set; }
        public string DeletedSpotifyPlaylist { get; set; }
        public string PlaylistItemRemove { get; set; }
    }

    public class InfoMessages
    {
        public string BotList { get; set; }
        public string BotStatus { get; set; }
        public string BotMessageTitle { get; set; }
        public string PlaylistItemAdded { get; set; }
        public string BotListDescription { get; set; }
        public string BotFooterSignature { get; set; }
        public string PlaylistItemRemoved { get; set; }
        public string ListenEncouragement { get; set; }
        public string PlaylistItemAddedDuplicate { get; set; }
        public string PlaylistReceivedUpdatesTitle { get; set; }
        public string PlaylistItemRemovedDescription { get; set; }
        public string PlaylistReceivedUpdatesDescription { get; set; }
    }

    public class LogMessages
    {
        public string BotStartup { get; set; }
        public string BotShutdown { get; set; }
        public string DatabaseStartup { get; set; }
        public string SpotifyApiTokenRefresh { get; set; }
    }

    public class Locale
    {
        public ErrorMessages ErrorMessages { get; set; }
        public InfoMessages InfoMessages { get; set; }
        public LogMessages LogMessages { get; set; }
    }

    public class Database
    {
        public string DatabaseName { get; set; }
        public string PlaylistCollectionName { get; set; }
        public string AuthenticationInformationCollectionName { get; set; }
    }

    public class Configuration
    {
        public Logging Logging { get; set; }
        public Locale Locale { get; set; }
        public Database Database { get; set; }
    }
}
﻿using System;

namespace Mz.Bot.Discord.Spotify.Models.Database
{
    internal class PlaylistDescription
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ulong GuildId { get; set; }
        public ulong ChannelId { get; set; }
        public string PlaylistId { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}

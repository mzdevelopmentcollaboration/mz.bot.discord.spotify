﻿namespace Mz.Bot.Discord.Spotify.Models.Discord
{
    internal class EmbedItem
    {
        public string EmbedItemTitle { get; set; }
        public string EmbedItemDescription { get; set; }
    }
}

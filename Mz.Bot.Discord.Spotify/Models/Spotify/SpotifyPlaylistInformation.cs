﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Mz.Bot.Discord.Spotify.Models.Spotify
{
    internal class SpotifyPlaylistInformation
    {
        [JsonProperty("items")] public List<PlaylistItem> Items { get; set; }
        [JsonProperty("total")] public int TotalItemCount { get; set; }

        internal class PlaylistItem
        {
            [JsonProperty("added_at")] public DateTime AddedAt { get; set; }
            [JsonProperty("track")] public TrackProperties TrackInformation { get; set; }

            internal class TrackProperties
            {
                [JsonProperty("external_urls")] public ExternalUrlCollection ExternalUrls { get; set; }
                [JsonProperty("name")] public string Name { get; set; }
                [JsonProperty("artists")] public List<Artist> Artists { get; set; }
                [JsonProperty("album")] public AlbumProperties Album { get; set; }

                internal class AlbumProperties
                {
                    [JsonProperty("images")] public List<ImageItem> Images { get; set; }

                    internal class ImageItem
                    {
                        [JsonProperty("height")] public string Height { get; set; }
                        [JsonProperty("width")] public string Width { get; set; }
                        [JsonProperty("url")] public string Url { get; set; }
                    }
                }

                internal class ExternalUrlCollection
                {
                    [JsonProperty("spotify")] public string SpotifyUrl { get; set; }
                }

                internal class Artist
                {
                    [JsonProperty("name")] public string Name { get; set; }
                }
            }
        }
    }
}

﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Mz.Bot.Discord.Spotify.Models.Spotify
{
    internal class SpotifyPlaylistDescription
    {
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("description")] public string Description { get; set; }
        [JsonProperty("images")] public List<SpotifyPlaylistInformation.PlaylistItem.TrackProperties.AlbumProperties.ImageItem> Images { get; set; }
        [JsonProperty("external_urls")] public SpotifyPlaylistInformation.PlaylistItem.TrackProperties.ExternalUrlCollection ExternalUrls { get; set; }
    }
}

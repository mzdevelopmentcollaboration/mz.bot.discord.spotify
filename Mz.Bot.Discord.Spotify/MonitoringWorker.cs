﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

using LiteDB;

using Mz.Bot.Discord.Spotify.Models.Discord;
using Mz.Bot.Discord.Spotify.Models.Spotify;
using Mz.Bot.Discord.Spotify.Methods.Spotify;
using Mz.Bot.Discord.Spotify.Methods.Discord;
using Mz.Bot.Discord.Spotify.Models.Database;
using Mz.Bot.Discord.Spotify.Models.Settings;

#pragma warning disable 4014

// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable MethodSupportsCancellation
// ReSharper disable PossibleMultipleEnumeration

namespace Mz.Bot.Discord.Spotify
{
    public class MonitoringWorker : BackgroundService
    {
        private readonly Configuration _config = new();
        private readonly ILogger<MonitoringWorker> _logger;
        private readonly DiscordEmbedManager _discordEmbedManager;

        public MonitoringWorker(ILogger<MonitoringWorker> logger, IConfiguration config)
        {
            _logger = logger;
            config.Bind(_config);
            _discordEmbedManager = new DiscordEmbedManager(_config.Locale);
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation(_config.Locale.LogMessages.DatabaseStartup);
            Thread.Sleep(5000);
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var spotifyApiManager = new SpotifyApiManager();
                using (var spotifyPlaylistDatabase = new LiteDatabase(Path.Combine(
                    Directory.GetCurrentDirectory(), "database", _config.Database.DatabaseName)))
                {
                    var spotifyPlaylistCollection =
                        spotifyPlaylistDatabase.GetCollection<PlaylistDescription>(_config.Database.PlaylistCollectionName);
                    var spotifyAuthenticationInformation = spotifyPlaylistDatabase
                        .GetCollection<SpotifyAuthenticationInformation>(_config.Database.AuthenticationInformationCollectionName)
                        .FindAll().First();
                    foreach (var spotifyPlaylist in spotifyPlaylistCollection.FindAll())
                    {
                        var spotifyPlaylistInformation = spotifyApiManager.FetchSpotifyPlaylistItems(
                            spotifyPlaylist.PlaylistId,
                            spotifyAuthenticationInformation.AccessToken);
                        if (spotifyPlaylistInformation == null)
                        {
                            _logger.LogWarning(_config.Locale.ErrorMessages.DeletedSpotifyPlaylist);
                            spotifyPlaylistCollection.Delete(spotifyPlaylist.Id);
                            continue;
                        }
                        var addedSpotifyPlaylistCollection = spotifyPlaylistInformation.Where(
                            x => x.AddedAt.ToLocalTime() > spotifyPlaylist.TimeStamp);
                        if (!addedSpotifyPlaylistCollection.Any())
                        {
                            continue;
                        }
                        var embedItemCollection = addedSpotifyPlaylistCollection.Select(addedSpotifyPlaylistItem =>
                            new EmbedItem
                            {
                                EmbedItemTitle = addedSpotifyPlaylistItem.TrackInformation.Name + " - " + 
                                    addedSpotifyPlaylistItem.TrackInformation.Artists[0].Name,
                                EmbedItemDescription = $"[{_config.Locale.InfoMessages.ListenEncouragement}]" + 
                                    $"({addedSpotifyPlaylistItem.TrackInformation.ExternalUrls.SpotifyUrl})"
                            }).ToList();
                        try
                        {
                            Program._discordSocketClient.GetGuild(spotifyPlaylist.GuildId)
                                .GetTextChannel(spotifyPlaylist.ChannelId)
                                .SendMessageAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(
                                    spotifyPlaylist.Title + _config.Locale.InfoMessages.PlaylistReceivedUpdatesTitle + ' ' +
                                    _config.Locale.InfoMessages.PlaylistReceivedUpdatesDescription,
                                    addedSpotifyPlaylistCollection.First().TrackInformation.Album.Images.First().Url,
                                    embedItemCollection));
                        }
                        catch (NullReferenceException deletedChannelException)
                        {
                            _logger.LogWarning(deletedChannelException,
                                _config.Locale.ErrorMessages.DeletedChannelTarget);
                            spotifyPlaylistCollection.Delete(spotifyPlaylist.Id);
                        }
                        spotifyPlaylist.TimeStamp = DateTime.UtcNow;
                        spotifyPlaylistCollection.Update(spotifyPlaylist);
                    }
                }
                await Task.Delay(60000, stoppingToken);
            }
        }
    }
}

﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using LiteDB;
using Discord;
using Discord.Commands;

using Mz.Bot.Discord.Spotify.Models.Spotify;
using Mz.Bot.Discord.Spotify.Models.Discord;
using Mz.Bot.Discord.Spotify.Methods.Discord;
using Mz.Bot.Discord.Spotify.Methods.Spotify;
using Mz.Bot.Discord.Spotify.Models.Database;
using Mz.Bot.Discord.Spotify.Models.Settings;

// ReSharper disable AssignNullToNotNullAttribute

namespace Mz.Bot.Discord.Spotify.Modules
{
    [Group("playlist")]
    public class PlaylistModule : ModuleBase<SocketCommandContext>
    {
        private readonly string _databasePath;
        private readonly Configuration _config;
        private readonly DiscordEmbedManager _discordEmbedManager;

        public PlaylistModule(Configuration config)
        {
            _config = config;
            _databasePath = Path.Combine(Directory.GetCurrentDirectory(),
                "database", _config.Database.DatabaseName);
            _discordEmbedManager = new DiscordEmbedManager(_config.Locale);
        }

        [Command("add")]
        [RequireUserPermission(ChannelPermission.ViewChannel)]
        public Task AddPlaylistItemAsync([Remainder] string spotifyPlaylistUri)
        {
            var spotifyLogicManager = new SpotifyLogicManager();
            var spotifyPlaylistId = spotifyLogicManager.ExtractSpotifyPlaylistId(new Uri(spotifyPlaylistUri));
            if (spotifyPlaylistId != string.Empty)
            {
                var spotifyApiManager = new SpotifyApiManager();
                using var spotifyPlaylistDatabase = new LiteDatabase(_databasePath);
                var spotifyAuthenticationInformation = spotifyPlaylistDatabase
                    .GetCollection<SpotifyAuthenticationInformation>(_config.Database.AuthenticationInformationCollectionName)
                    .FindAll().First();
                var spotifyPlaylistInformation = spotifyApiManager.FetchSpotifyPlaylistDescription(spotifyPlaylistId,
                    spotifyAuthenticationInformation.AccessToken);
                if (spotifyPlaylistInformation == null)
                {
                    ReplyAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(_config.Locale.ErrorMessages.PlaylistItemAdd,
                        string.Empty, new List<EmbedItem>()));
                    return Task.CompletedTask;
                }
                var spotifyPlaylistCollection =
                    spotifyPlaylistDatabase.GetCollection<PlaylistDescription>(_config.Database.PlaylistCollectionName);
                var spotifyPlaylistItem = new PlaylistDescription()
                {
                    Title = spotifyPlaylistInformation.Name,
                    PlaylistId = spotifyPlaylistId,
                    GuildId = Context.Guild.Id,
                    ChannelId = Context.Channel.Id,
                    TimeStamp = DateTime.UtcNow
                };
                if (spotifyPlaylistCollection.Find(x => x.Title == spotifyPlaylistItem.Title).Any() &&
                    spotifyPlaylistCollection.Find(x => x.GuildId == spotifyPlaylistItem.GuildId).Any() &&
                    spotifyPlaylistCollection.Find(x => x.ChannelId == spotifyPlaylistItem.ChannelId).Any())
                {
                    ReplyAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(
                        _config.Locale.InfoMessages.PlaylistItemAddedDuplicate, string.Empty,
                        new List<EmbedItem>()));
                }
                else
                {
                    spotifyPlaylistCollection.Insert(spotifyPlaylistItem);
                    ReplyAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(spotifyPlaylistItem.Title +
                        _config.Locale.InfoMessages.PlaylistItemAdded + " - " + spotifyPlaylistInformation.Description + " - " +
                        $"[{_config.Locale.InfoMessages.ListenEncouragement}]({spotifyPlaylistInformation.ExternalUrls.SpotifyUrl})",
                        spotifyPlaylistInformation.Images.First().Url,new List<EmbedItem>()));
                }
            }
            else
            {
                ReplyAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(
                    _config.Locale.ErrorMessages.PlaylistItemAdd, string.Empty,
                    new List<EmbedItem>()));
            }
            return Task.CompletedTask;
        }

        [Command("remove")]
        [RequireUserPermission(ChannelPermission.ViewChannel)]
        public Task RemovePlaylistItemAsync([Remainder] string spotifyPlaylistTitle)
        {
            using var spotifyPlaylistDatabase = new LiteDatabase(_databasePath);
            var spotifyPlaylistCollection = spotifyPlaylistDatabase
                .GetCollection<PlaylistDescription>(_config.Database.PlaylistCollectionName);
            var spotifyPlaylistItem = spotifyPlaylistCollection.FindOne(x =>
                x.Title == spotifyPlaylistTitle && x.GuildId == Context.Guild.Id && x.ChannelId == Context.Channel.Id);
            if (spotifyPlaylistItem == null)
            {
                ReplyAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(_config.Locale.ErrorMessages.PlaylistItemRemove,
                    string.Empty, new List<EmbedItem>()));
                return Task.CompletedTask;
            }
            spotifyPlaylistCollection.Delete(spotifyPlaylistItem.Id);
            ReplyAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(
                spotifyPlaylistItem.Title + _config.Locale.InfoMessages.PlaylistItemRemoved + ' ' +
                _config.Locale.InfoMessages.PlaylistItemRemovedDescription, string.Empty,
                new List<EmbedItem>()));
            return Task.CompletedTask;
        }

        [Command("list")]
        [RequireUserPermission(ChannelPermission.ViewChannel)]
        public Task ListPlaylistItemAsync()
        {
            using var spotifyPlaylistDatabase = new LiteDatabase(_databasePath);
            var spotifyPlaylistCollection = spotifyPlaylistDatabase
                .GetCollection<PlaylistDescription>(_config.Database.PlaylistCollectionName);
            var embedItemList = spotifyPlaylistCollection.Find(x =>
                    x.GuildId == Context.Guild.Id && x.ChannelId == Context.Channel.Id)
                .Select(spotifyPlaylistItem => new EmbedItem { EmbedItemTitle = spotifyPlaylistItem.Title,
                    EmbedItemDescription = _config.Locale.InfoMessages.BotListDescription + spotifyPlaylistItem.TimeStamp }).ToList();
            ReplyAsync(embed: _discordEmbedManager.GenerateUnifiedEmbed(
                _config.Locale.InfoMessages.BotList, string.Empty, embedItemList));
            return Task.CompletedTask;
        }
    }
}

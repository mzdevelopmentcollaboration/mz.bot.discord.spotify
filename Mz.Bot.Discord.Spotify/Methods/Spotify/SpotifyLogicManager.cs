﻿using System;

namespace Mz.Bot.Discord.Spotify.Methods.Spotify
{
    internal class SpotifyLogicManager
    {
        public string ExtractSpotifyPlaylistId(Uri spotifyPlaylistUri)
        {
            try
            {
                return spotifyPlaylistUri.Segments[2];
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}

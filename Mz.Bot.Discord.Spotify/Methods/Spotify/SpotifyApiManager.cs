﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Generic;

using Newtonsoft.Json;

using Mz.Bot.Discord.Spotify.Models.Spotify;

namespace Mz.Bot.Discord.Spotify.Methods.Spotify
{
    internal class SpotifyApiManager
    {

        public SpotifyAuthenticationInformation FetchSpotifyApiAuthenticationToken(string spotifyApiClientId, string spotifyApiClientSecret)
        {
            var authorizationStringBytes = Encoding.UTF8.GetBytes($"{spotifyApiClientId}:{spotifyApiClientSecret}");
            var webRequestParameter = Encoding.UTF8.GetBytes("grant_type=client_credentials");
            var webRequest = WebRequest.Create("https://accounts.spotify.com/api/token");
            webRequest.Method = "POST";
            webRequest.Headers.Add("Authorization", $"Basic {Convert.ToBase64String(authorizationStringBytes)}");
            webRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            var webRequestStream = webRequest.GetRequestStream();
            webRequestStream.Write(webRequestParameter, 0, webRequestParameter.Length);
            webRequestStream.Close();
            var webRequestResponse = webRequest.GetResponse();
            var webRequestResponseStream = webRequestResponse.GetResponseStream();
            using var webRequestResponseStreamReader = new StreamReader(webRequestResponseStream ??
                throw new InvalidOperationException("Response-Stream during Spotify-API-Interaction was Null!"));
            return JsonConvert.DeserializeObject<SpotifyAuthenticationInformation>(webRequestResponseStreamReader.ReadToEnd());
        }

        public SpotifyPlaylistDescription FetchSpotifyPlaylistDescription(string playlistId, string spotifyApiAuthenticationToken)
        {
            var webRequestEndpoint = $"https://api.spotify.com/v1/playlists/{playlistId}" + '?' +
                "fields=description,external_urls,name,images";
            var webRequest = WebRequest.Create(webRequestEndpoint);
            webRequest.Headers.Add("Authorization", $"Bearer {spotifyApiAuthenticationToken}");
            try
            {
                var webRequestResponse = webRequest.GetResponse();
                var webRequestResponseStream = webRequestResponse.GetResponseStream();
                using var webRequestResponseStreamReader = new StreamReader(webRequestResponseStream ??
                    throw new InvalidOperationException("Response-Stream during Spotify-API-Interaction was Null!"));
                return JsonConvert.DeserializeObject<SpotifyPlaylistDescription>(webRequestResponseStreamReader.ReadToEnd());
            }
            catch
            {
                return null;
            }
        }

        //TODO: Fix 101-Problem
        public List<SpotifyPlaylistInformation.PlaylistItem> FetchSpotifyPlaylistItems(string playlistId,
            string spotifyApiAuthenticationToken)
        {
            var initialWebRequestEndpoint = $"https://api.spotify.com/v1/playlists/{playlistId}/tracks" + '?' +
                "total,items(added_at,track.album.images,track.artists.name,track.external_urls,track.name)";
            var webRequest = WebRequest.Create(initialWebRequestEndpoint);
            webRequest.Headers.Add("Authorization", $"Bearer {spotifyApiAuthenticationToken}");
            try
            {
                var webRequestResponse = webRequest.GetResponse();
                var webRequestResponseStream = webRequestResponse.GetResponseStream();
                using var webRequestResponseStreamReader = new StreamReader(webRequestResponseStream ??
                    throw new InvalidOperationException("Response-Stream during Spotify-API-Interaction was Null!"));
                var initialSpotifyPlaylistInformation = JsonConvert
                    .DeserializeObject<SpotifyPlaylistInformation>(webRequestResponseStreamReader.ReadToEnd());
                if (initialSpotifyPlaylistInformation.TotalItemCount <= 100)
                {
                    return initialSpotifyPlaylistInformation.Items;
                }
                var pagedWebRequest = WebRequest.Create(initialWebRequestEndpoint +
                    $"&offset={initialSpotifyPlaylistInformation.TotalItemCount - 100}");
                pagedWebRequest.Headers.Add("Authorization", $"Bearer {spotifyApiAuthenticationToken}");
                var pagedWebRequestResponse = pagedWebRequest.GetResponse();
                var pagedWebRequestResponseStream = pagedWebRequestResponse.GetResponseStream();
                using var pagedWebRequestResponseStreamReader = new StreamReader(pagedWebRequestResponseStream ??
                    throw new InvalidOperationException("Response-Stream during Spotify-API-Interaction was Null!"));
                return JsonConvert.DeserializeObject<SpotifyPlaylistInformation>(pagedWebRequestResponseStreamReader
                    .ReadToEnd()).Items;
            }
            catch
            {
                return new List<SpotifyPlaylistInformation.PlaylistItem>();
            }
        }
    }
}

﻿using System.Collections.Generic;

using Discord;

using Mz.Bot.Discord.Spotify.Models.Discord;
using Mz.Bot.Discord.Spotify.Models.Settings;

namespace Mz.Bot.Discord.Spotify.Methods.Discord
{
    internal class DiscordEmbedManager
    {
        private readonly Locale _locale;

        public DiscordEmbedManager(Locale locale)
        {
            _locale = locale;
        }

        public Embed GenerateUnifiedEmbed(string descriptionText,
            string imageUrl, List<EmbedItem> embedItemCollection)
        {
            var unifiedEmbedBuilder = new EmbedBuilder
            {
                Title = _locale.InfoMessages.BotMessageTitle,
                Description = descriptionText
            };
            unifiedEmbedBuilder.WithFooter(_locale.InfoMessages.BotFooterSignature)
                .WithCurrentTimestamp()
                .WithColor(Color.Green)
                .WithImageUrl(imageUrl);
            foreach (var embedItem in embedItemCollection)
            {
                unifiedEmbedBuilder.AddField(embedItem.EmbedItemTitle, embedItem.EmbedItemDescription);
            }
            return unifiedEmbedBuilder.Build();
        }
    }
}
